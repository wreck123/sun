package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.Stulist;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
public interface StulistMapper extends BaseMapper<Stulist> {

}
