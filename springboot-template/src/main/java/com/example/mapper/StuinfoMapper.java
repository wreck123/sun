package com.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.entity.Stuinfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
public interface StuinfoMapper extends BaseMapper<Stuinfo> {

}
