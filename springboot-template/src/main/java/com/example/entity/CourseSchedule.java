package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CourseSchedule对象", description="课程进度表")
@TableName(value = "course_schedule")
public class CourseSchedule implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "序号")
    private String no;

    @ApiModelProperty(value = "开始签到")
    private Date bSigninTime;

    @ApiModelProperty(value = "识别图片")
    private Blob image;

    @ApiModelProperty(value = "出勤率")
    private String attendrate;

    @ApiModelProperty(value = "应到学生")
    private String courseName;


}
