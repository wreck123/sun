package com.example.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Teachinfo对象", description="教师信息表")
@TableName(value = "teachinfo")
public class Teachinfo implements Serializable {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "教师编号")
    private String teachNo;

    @ApiModelProperty(value = "教师名称")
    private String teachName;

    @ApiModelProperty(value = "研究方向")
    private String teachSearch;

    @ApiModelProperty(value = "研究方向")
    private String teachRank;

    @ApiModelProperty(value = "教授课程名")
    private String courseName;


}
