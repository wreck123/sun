package com.example.controller;


import com.example.service.StusigninService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
@RestController
@RequestMapping("/example/stusignin")
public class StusigninController {
    @Resource
    private StusigninService stusigninService;

}

