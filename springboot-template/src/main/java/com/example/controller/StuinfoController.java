package com.example.controller;


import com.example.entity.Stuinfo;
import com.example.service.StuinfoService;
import com.example.utils.ResultVoUtil;
import com.example.vo.ResultVo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
@RestController
@RequestMapping("/example/stuinfo")
public class StuinfoController {

    @Resource
    private StuinfoService stuinfoService;

    @PostMapping(value = "list")
    public ResultVo queryAll(){
        List<Stuinfo> list = stuinfoService.queryAll();
        return ResultVoUtil.success(list);
    }



}

