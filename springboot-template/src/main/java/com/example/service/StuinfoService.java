package com.example.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.entity.Stuinfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
public interface StuinfoService extends IService<Stuinfo> {

    List<Stuinfo> queryAll();
}
