package com.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.entity.Teachinfo;
import com.example.mapper.TeachinfoMapper;
import com.example.service.TeachinfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
@Service
public class TeachinfoServiceImpl extends ServiceImpl<TeachinfoMapper, Teachinfo> implements TeachinfoService {
    @Resource
    private TeachinfoMapper teachinfoMapper;

}
