package com.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.entity.SigninHistory;
import com.example.mapper.SigninHistoryMapper;
import com.example.service.SigninHistoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
@Service
public class SigninHistoryServiceImpl extends ServiceImpl<SigninHistoryMapper, SigninHistory> implements SigninHistoryService {
    @Resource
    private SigninHistoryMapper signinHistoryMapper;

}
