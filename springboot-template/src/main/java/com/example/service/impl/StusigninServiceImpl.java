package com.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.entity.Stusignin;
import com.example.mapper.StusigninMapper;
import com.example.service.StusigninService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
@Service
public class StusigninServiceImpl extends ServiceImpl<StusigninMapper, Stusignin> implements StusigninService {
    @Resource
    private StusigninMapper stusigninMapper;
}
