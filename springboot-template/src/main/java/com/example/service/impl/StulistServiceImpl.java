package com.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.entity.Stulist;
import com.example.mapper.StulistMapper;
import com.example.service.StulistService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
@Service
public class StulistServiceImpl extends ServiceImpl<StulistMapper, Stulist> implements StulistService {
    @Resource
    private StulistMapper stulistMapper;
}
