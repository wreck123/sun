package com.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.entity.Stucourse;
import com.example.mapper.StucourseMapper;
import com.example.service.StucourseService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
@Service
public class StucourseServiceImpl extends ServiceImpl<StucourseMapper, Stucourse> implements StucourseService {
    @Resource
    private StucourseMapper stucourseMapper;

}
