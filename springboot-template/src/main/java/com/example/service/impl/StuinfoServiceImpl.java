package com.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.example.entity.Stuinfo;
import com.example.mapper.StuinfoMapper;
import com.example.service.StuinfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
@Service
public class StuinfoServiceImpl extends ServiceImpl<StuinfoMapper, Stuinfo> implements StuinfoService {
        @Resource
        private StuinfoMapper stuinfoMapper;

        @Override
        public List<Stuinfo> queryAll() {
                return stuinfoMapper.selectList(null);
        }
}
