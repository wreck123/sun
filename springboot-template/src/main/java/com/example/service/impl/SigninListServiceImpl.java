package com.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.entity.SigninList;
import com.example.mapper.SigninListMapper;
import com.example.service.SigninListService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
@Service
public class SigninListServiceImpl extends ServiceImpl<SigninListMapper, SigninList> implements SigninListService {
    @Resource
    private SigninListMapper signinListMapper;

}
