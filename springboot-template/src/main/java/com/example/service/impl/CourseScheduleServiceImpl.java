package com.example.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.entity.CourseSchedule;
import com.example.mapper.CourseScheduleMapper;
import com.example.service.CourseScheduleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author sh
 * @since 2020-12-08
 */
@Service
public class CourseScheduleServiceImpl extends ServiceImpl<CourseScheduleMapper, CourseSchedule> implements CourseScheduleService {
    @Resource
    private CourseScheduleMapper courseScheduleMapper;

}
